prefix = /usr/local

include Makefile.conf

SHELL := /bin/bash

TAG ?= $(shell git describe --tags --always)

PANDOC_DOWNLOAD_URL=https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
PANDOC_CROSSREF_DOWNLOAD_URL=https://github.com/lierdakil/pandoc-crossref/releases/download/v$(PANDOC_CROSSREF_VERSION)/pandoc-crossref-Linux.tar.xz

PANDOC=howdyadoc/bin/pandoc howdyadoc/man/pandoc.1.gz
PANDOC_CROSSREF=howdyadoc/bin/pandoc-crossref howdyadoc/man/pandoc-crossref.1

CMDS2LINK=$(shell find howdyadoc/ -maxdepth 1 -type f)
LINKTARGET_PREFIX=$(prefix)/lib
INSTALL_PREFIX=$(DESTDIR)$(prefix)/lib
CMDLINK_PREFIX= $(DESTDIR)$(prefix)/bin

DIRS=$(shell find howdyadoc -type d)
INSTALL_DIRS=$(foreach DIR, $(DIRS), $(INSTALL_PREFIX)/$(DIR))

download: $(PANDOC) $(PANDOC_CROSSREF)

clean:
		- rm $(PANDOC) $(PANDOC_CROSSREF)
		- rm -Rf howdyadoc/{bin,man}
		- rm howdyadoc_$(TAG)-1_amd64.deb

install: $(PANDOC) $(PANDOC_CROSSREF) $(INSTALL_DIRS) $(CMDLINK_PREFIX)
	$(foreach DIR, $(DIRS), install $(DIR)/* $(INSTALL_PREFIX)/$(DIR);)
	$(foreach CMD, $(CMDS2LINK), ln -fs $(LINKTARGET_PREFIX)/$(CMD) $(CMDLINK_PREFIX)/$(notdir $(CMD));)

uninstall:
	- rm -Rf $(INSTALL_PREFIX)/howdyadoc
	- rm $(foreach CMD, $(CMDS2LINK), $(CMDLINK_PREFIX)/$(notdir $(CMD)))

debian: howdyadoc_$(TAG)-1_amd64.deb

howdyadoc_$(TAG)-1_amd64.deb:
	$(eval tmpdir := $(shell mktemp -d))
	mkdir $(tmpdir)/howdyadoc-$(TAG)
	rsync -ar ./ $(tmpdir)/howdyadoc-$(TAG)
	cd $(tmpdir)/howdyadoc-$(TAG) && make clean && dh_clean
	cd $(tmpdir) && tar -czf howdyadoc_$(TAG).orig.tar.gz howdyadoc-$(TAG)
	cd $(tmpdir)/howdyadoc-$(TAG) && debuild -us -uc
	cp $(tmpdir)/howdyadoc_$(TAG)-1_amd64.deb .
	rm -Rf $(tmpdir)

$(PANDOC):
	mkdir howdyadoc/{bin,man}
	cd howdyadoc; \
	wget $(PANDOC_DOWNLOAD_URL) -O - | tar -xz --strip-components=1; \
	mv share/man/man1/pandoc.1.gz man/ ; \
	rm -R share

$(PANDOC_CROSSREF):
	cd howdyadoc; \
	wget $(PANDOC_CROSSREF_DOWNLOAD_URL) -O - | tar -xJC bin ; \
	mv bin/pandoc-crossref.1 man/

$(INSTALL_DIRS) $(CMDLINK_PREFIX):
	mkdir -p $(INSTALL_DIRS) $(CMDLINK_PREFIX)
